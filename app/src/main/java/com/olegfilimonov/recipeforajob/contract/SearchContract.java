package com.olegfilimonov.recipeforajob.contract;

import com.olegfilimonov.recipeforajob.model.Recipe;
import com.olegfilimonov.recipeforajob.mvp.BasePresenter;
import com.olegfilimonov.recipeforajob.mvp.BaseView;
import java.util.List;

/**
 * @author Oleg Filimonov
 */

public class SearchContract {

  public interface Presenter extends BasePresenter {

    void loadRecipies(String query);
  }

  public interface View extends BaseView<Presenter> {

    void clearSearchResults();

    void showSearchResults(List<Recipe> recipes);

    void showEmptyResults(int stringRes);

    void hideEmptyText();
  }
}
