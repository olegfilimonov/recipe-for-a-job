package com.olegfilimonov.recipeforajob.presenter;

import com.birbit.android.jobqueue.JobManager;
import com.birbit.android.jobqueue.Params;
import com.birbit.android.jobqueue.TagConstraint;
import com.olegfilimonov.recipeforajob.R;
import com.olegfilimonov.recipeforajob.contract.SearchContract;
import com.olegfilimonov.recipeforajob.model.Recipe;
import com.olegfilimonov.recipeforajob.mvp.UseCase;
import com.olegfilimonov.recipeforajob.singleton.Constant;
import com.olegfilimonov.recipeforajob.singleton.RecipeApplication;
import com.olegfilimonov.recipeforajob.usecase.GetRecipesJob;
import java.util.List;
import java.util.UUID;
import javax.inject.Inject;

/**
 * @author Oleg Filimonov
 */

public class SearchPresenter implements SearchContract.Presenter {
  /**
   * A tag that is unique to this presenter instance. Can be used to cancel all jobs of the
   * presenter at once
   */
  private final String presenterTag = UUID.randomUUID().toString();
  @Inject JobManager jobManager;
  private SearchContract.View view;

  public SearchPresenter(SearchContract.View view) {
    this.view = view;
    RecipeApplication.getInstance().getComponent().inject(this);
    view.setPresenter(this);
  }

  @Override public void loadRecipies(String query) {

    view.hideEmptyText();

    if (query.trim().equals("")) {
      // show empty results
      view.clearSearchResults();
      view.showEmptyResults(R.string.search_no_query);
      return;
    }

    // Cancel all previous jobs
    jobManager.cancelJobsInBackground(null, TagConstraint.ALL, presenterTag);

    UseCase.UseCaseCallback<GetRecipesJob.ResponseValue> callback =
        new UseCase.UseCaseCallback<GetRecipesJob.ResponseValue>() {
          @Override public void onSuccess(GetRecipesJob.ResponseValue response) {
            List<Recipe> recipes = response.getGifs();
            view.showSearchResults(recipes);

            if (recipes.size() == 0) {
              view.showEmptyResults(R.string.search_empty);
            }
          }

          @Override public void onError() {
            // TODO: 23/08/2017 Add error handling here
            //view.showError();
          }
        };

    GetRecipesJob job = new GetRecipesJob(new GetRecipesJob.RequestValues(query), callback,
        new Params(Constant.DEFAULT_PRIORITY).addTags(presenterTag));

    jobManager.addJobInBackground(job);
  }
}
