package com.olegfilimonov.recipeforajob.client;

import com.olegfilimonov.recipeforajob.model.RecipePuppyRequestResult;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

import static com.olegfilimonov.recipeforajob.singleton.Constant.API;

/**
 * @author Oleg Filimonov
 */
public interface DefaultApi {

  @GET(API) Call<RecipePuppyRequestResult> getRecipes(@Query("q") String querry);
}