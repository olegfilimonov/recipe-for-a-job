package com.olegfilimonov.recipeforajob.dagger;

import com.olegfilimonov.recipeforajob.client.ApiClient;
import com.olegfilimonov.recipeforajob.client.DefaultApi;
import dagger.Module;
import dagger.Provides;
import javax.inject.Singleton;

/**
 * Dagger module for everything API related
 *
 * @author Oleg Filimonov
 */

@Module public class ApiModule {

  public ApiModule() {
  }

  @Provides @Singleton ApiClient provideApiClient() {
    return new ApiClient();
  }

  @Provides @Singleton DefaultApi provideDefaultApi(ApiClient apiClient) {
    return apiClient.createService(DefaultApi.class);
  }
}
