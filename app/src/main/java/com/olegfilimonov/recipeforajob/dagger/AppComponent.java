package com.olegfilimonov.recipeforajob.dagger;

import com.olegfilimonov.recipeforajob.mvp.UseCase;
import com.olegfilimonov.recipeforajob.presenter.SearchPresenter;
import dagger.Component;
import javax.inject.Singleton;

/**
 * @author Oleg Filimonov
 */
@Singleton @Component(modules = { AppModule.class, ApiModule.class })
public interface AppComponent {
  void inject(SearchPresenter presenter);

  void inject(UseCase<UseCase.RequestValues, UseCase.ResponseValue> useCase);
}
