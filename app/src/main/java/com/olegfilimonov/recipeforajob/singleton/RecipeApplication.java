package com.olegfilimonov.recipeforajob.singleton;

import android.app.Application;
import com.olegfilimonov.recipeforajob.dagger.ApiModule;
import com.olegfilimonov.recipeforajob.dagger.AppComponent;
import com.olegfilimonov.recipeforajob.dagger.AppModule;
import com.olegfilimonov.recipeforajob.dagger.DaggerAppComponent;

/**
 * @author Oleg Filimonov
 */

public class RecipeApplication extends Application {

  private static RecipeApplication instance;
  private AppComponent component;

  public static synchronized RecipeApplication getInstance() {
    return instance;
  }

  @Override public void onCreate() {
    super.onCreate();
    instance = this;

    component = DaggerAppComponent.builder()
        .appModule(new AppModule(getApplicationContext()))
        .apiModule(new ApiModule())
        .build();
  }

  public AppComponent getComponent() {
    return component;
  }
}

