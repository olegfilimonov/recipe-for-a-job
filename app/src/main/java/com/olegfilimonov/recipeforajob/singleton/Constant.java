package com.olegfilimonov.recipeforajob.singleton;

import com.olegfilimonov.recipeforajob.BuildConfig;

/**
 * @author Oleg Filimonov
 */

public class Constant {

  /* DEBUGGING */
  public static final boolean DEBUG = BuildConfig.BUILD_TYPE.equals("debug");

  /* NETWORKING */
  public static final String BASE_URL = "http://www.recipepuppy.com";
  public static final String API = "/api/";

  /* SEARCH */
  public static final int SEARCH_LIMIT = 20;

  /* JOBS */
  public static final int DEFAULT_PRIORITY = 0;
}
