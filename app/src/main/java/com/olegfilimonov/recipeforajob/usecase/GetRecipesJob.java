package com.olegfilimonov.recipeforajob.usecase;

import android.support.annotation.Nullable;
import com.birbit.android.jobqueue.Params;
import com.olegfilimonov.recipeforajob.model.Recipe;
import com.olegfilimonov.recipeforajob.model.RecipePuppyRequestResult;
import com.olegfilimonov.recipeforajob.mvp.UseCase;
import java.util.ArrayList;
import java.util.List;
import retrofit2.Call;
import retrofit2.Response;

/**
 * @author Oleg Filimonov
 */

public class GetRecipesJob
    extends UseCase<GetRecipesJob.RequestValues, GetRecipesJob.ResponseValue> {

  public GetRecipesJob(RequestValues requestValues,
      UseCase.UseCaseCallback<ResponseValue> useCaseCallback, Params params) {
    super(requestValues, useCaseCallback, params);
  }

  @Override protected void executeUseCase(RequestValues requestValues) throws Throwable {

    // Get list of gifs
    Call<RecipePuppyRequestResult> call = defaultApi.getRecipes(requestValues.getQuery());
    Response<RecipePuppyRequestResult> response = call.execute();

    if (response.isSuccessful() && !isCancelled()) {

      // Get results

      List<Recipe> recipes =
          response.body() == null ? new ArrayList<Recipe>() : response.body().getRecipes();

      onSuccess(new ResponseValue(recipes));
    } else {
      onError();
    }
  }

  @Override protected void onCancel(int cancelReason, @Nullable Throwable throwable) {
    // Do nothing
  }

  public static final class RequestValues implements UseCase.RequestValues {
    private String query;

    public RequestValues(String query) {
      this.query = query;
    }

    public String getQuery() {
      return query;
    }

    public void setQuery(String query) {
      this.query = query;
    }
  }

  public static final class ResponseValue implements UseCase.ResponseValue {

    private final List<Recipe> gifs;

    public ResponseValue(List<Recipe> gifs) {
      this.gifs = gifs;
    }

    public List<Recipe> getGifs() {
      return gifs;
    }
  }
}
