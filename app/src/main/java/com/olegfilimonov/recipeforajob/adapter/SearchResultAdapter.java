package com.olegfilimonov.recipeforajob.adapter;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.olegfilimonov.recipeforajob.R;
import com.olegfilimonov.recipeforajob.model.Recipe;
import java.util.List;

/**
 * @author Oleg Filimonov
 */

public class SearchResultAdapter
    extends RecyclerView.Adapter<SearchResultAdapter.SearchResultViewHolder> {
  private List<Recipe> recipes;
  private Activity activity;

  public SearchResultAdapter(List<Recipe> recipes, Activity activity) {
    this.recipes = recipes;
    this.activity = activity;
  }

  @Override public SearchResultViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
    View view = LayoutInflater.from(parent.getContext())
        .inflate(R.layout.item_search_result, parent, false);
    return new SearchResultViewHolder(view);
  }

  @Override public void onBindViewHolder(final SearchResultViewHolder holder, int position) {
    final Recipe recipe = recipes.get(position);
    holder.title.setText(recipe.getTitle());
  }

  @Override public int getItemCount() {
    return recipes.size();
  }

  public void clearResults() {
    notifyItemRangeRemoved(0, recipes.size());
    recipes.clear();
  }

  public void setItems(List<Recipe> newRecipies) {
    clearResults();
    notifyItemRangeInserted(0, newRecipies.size());
    recipes.addAll(newRecipies);
  }

  class SearchResultViewHolder extends RecyclerView.ViewHolder {
    @BindView(R.id.search_result_title) TextView title;

    SearchResultViewHolder(View itemView) {
      super(itemView);
      ButterKnife.bind(this, itemView);
    }
  }
}
