package com.olegfilimonov.recipeforajob.activity;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.arlib.floatingsearchview.FloatingSearchView;
import com.olegfilimonov.recipeforajob.R;
import com.olegfilimonov.recipeforajob.adapter.SearchResultAdapter;
import com.olegfilimonov.recipeforajob.contract.SearchContract;
import com.olegfilimonov.recipeforajob.model.Recipe;
import com.olegfilimonov.recipeforajob.presenter.SearchPresenter;
import java.util.ArrayList;
import java.util.List;

public class SearchActivity extends AppCompatActivity implements SearchContract.View {
  @BindView(R.id.search_recycler_view) RecyclerView searchRecyclerView;
  @BindView(R.id.empty_text_view) TextView emptyTextView;
  @BindView(R.id.floating_search_view) FloatingSearchView floatingSearchView;

  private SearchContract.Presenter presenter;
  private SearchResultAdapter searchAdapter;

  @Override protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_search);
    ButterKnife.bind(this);

    setPresenter(new SearchPresenter(this));

    setupSearchView();
    setupRecyclerView();
  }

  private void setupRecyclerView() {
    searchRecyclerView.setLayoutManager(new LinearLayoutManager(this));
    searchAdapter = new SearchResultAdapter(new ArrayList<Recipe>(), this);
    searchRecyclerView.setAdapter(searchAdapter);
    searchRecyclerView.setHasFixedSize(true);
    searchRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
      @Override public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
        super.onScrollStateChanged(recyclerView, newState);
        // Hide keyboard
        InputMethodManager imm =
            (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(recyclerView.getWindowToken(), 0);
      }
    });
  }

  private void setupSearchView() {
    floatingSearchView.setOnQueryChangeListener(new FloatingSearchView.OnQueryChangeListener() {
      @Override public void onSearchTextChanged(String oldQuery, String newQuery) {
        presenter.loadRecipies(newQuery);
      }
    });
  }

  @Override public void clearSearchResults() {
    searchAdapter.clearResults();
  }

  @Override public void showSearchResults(List<Recipe> recipes) {
    searchAdapter.setItems(recipes);
  }

  @Override public void showEmptyResults(int stringRes) {
    searchRecyclerView.setVisibility(View.GONE);
    emptyTextView.setVisibility(View.VISIBLE);
    emptyTextView.setText(stringRes);
  }

  @Override public void hideEmptyText() {
    searchRecyclerView.setVisibility(View.VISIBLE);
    emptyTextView.setVisibility(View.GONE);
  }

  @Override public void setPresenter(SearchContract.Presenter presenter) {
    this.presenter = presenter;
  }
}
